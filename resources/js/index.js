$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal', function (e){
        console.log('El Modal se Esta Mostrando');  
        $('#btnsubs').prop('disabled', true);
        document.getElementById('btnsubs').style.backgroundColor='blue';
    })
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('El Modal se Mostro Correctamente');  
    })
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('El Modal Contacto se Oculta');  
    })
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('El Modal Contacto se Mostro'); 
        $('#btnsubs').prop('disabled', false); 
        document.getElementById('btnsubs').style.backgroundColor = '#8f0302';
    })
});