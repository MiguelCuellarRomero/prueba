module.exports = function(grunt){

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'resources/css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css',
                }]
            }
        },

        watch:{
            files: ['resources/css/*.scss'],
            tasks: ['css']
        },

        browserSync:{
            dev: {
                bsFiles: {
                    src: [
                        'resources/css/*.css',
                        'index.html',
                        'resources/views/*.html',
                        'resources/js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './'
                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'resources/assets/images/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/resources/assets/images'
                }]
            }
        },

        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['index.html' , 'resources/views/*.html'],
                    dest: 'dist/resources/views'
                }]
            },
            fonts: [{
                expand: true,
                dot: true,
                cwd: 'node_modules/open-iconic/font',
                src: ['fonts/*.*'],
                dest: 'dist'
            }]
        },

        clean: {
            build: {
                src: 'dist/'
            }
        },

        cssmin: {
            dist: {}
        },

        uglify: {
            dist: {}
        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                files: [{
                    src: ['dist/resources/js/*.js' , 'dist/resources/css/*.css']
                }]
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 
                'resources/views/albumes',
                'resources/views/artistas',
                'resources/views/canciones',
                'resources/views/contacto',
                'resources/views/noticias'
                ]
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block){
                                let generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin:{
            html: [
                'dist/index.html', 
                'dist/resources/views/albumes',
                'dist/resources/views/artistas',
                'dist/resources/views/canciones',
                'dist/resources/views/contacto',
                'dist/resources/views/noticias'
            ],
            options: {
                assetsDir: ['dist', 'dist/resources/css', 'dist/resources/js']
            }
        }

    });

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', ['clean', 'copy', 'imagemin', 'useminPrepare', 'concat', 'cssmin', 'uglify', 'filerev', 'usemin'])
};  